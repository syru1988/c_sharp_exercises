﻿using System;
using System.IO;
using System.IO.Compression;

public class Program
{
    public static void Main()
    {        
        Console.WriteLine("Podaj ile plików spakować: ");        
        int ile = Convert.ToInt32(Console.ReadLine());
        string[] plik = new string[ile];
        Console.WriteLine("Podaj katalog źródłowy:");
        string katalog = Console.ReadLine();
        string temp = Path.Combine(katalog, @"temp\");
        Directory.CreateDirectory(temp);

        for (int i=0; i<ile; i++)
        {
            Console.WriteLine("Podaj nazwę pliku {0}: ", i+1);
            plik[i] = Console.ReadLine();
            string zrodlo = Path.Combine(katalog, plik[i]);
            string destyn = $@"{katalog}temp\{plik[i]}";
            File.Copy(zrodlo, destyn, true);
        }
        string spakowany = $"{katalog}{DateTime.Now.Hour}{DateTime.Now.Minute}{DateTime.Now.Second}spakowany.zip"; //uniq name

        ZipFile.CreateFromDirectory(temp, spakowany); 

        Directory.Delete(temp, true); //cleaning after myself
    }
}