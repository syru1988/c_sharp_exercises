﻿using System;
using System.Security.Cryptography;

namespace Krypografia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generated key:");
            TripleDESCng tdes = new TripleDESCng();
            tdes.GenerateKey();

            Console.WriteLine(Convert.ToBase64String(tdes.Key));
                        
            Console.WriteLine("\nPress any key to exit");            
            Console.ReadKey();
        }
    }
}
