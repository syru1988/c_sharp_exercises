﻿using System;

namespace Indeksator
{
    class Program
    {
        public class TablicaLiter
        {
            // Array of leters chars
            char[] letter = new char[10]
            {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
            };

            // Indexer declaration.            
            public char this[int index]
            {
                get
                {
                    return letter[index];
                }
                set
                {
                    letter[index] = value;
                } 
            }
        }

        static void Main(string[] args)
        {
            var litera = new TablicaLiter();

            // Use the indexer's set accessor
            litera[3] = 'z';
            
            // Use the indexer's get accessor
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Position {0} = {1}", i, litera[i]);
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}


