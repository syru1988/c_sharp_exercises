﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZadanieDomowe3
{
    class Program
    {
        static void Main(string[] args)
        {
            var slowa = new List<string>();

            Console.WriteLine("Podaj 20 słów");
            var words  = Console.ReadLine();
            string[] singleWord = words.Split(' ');

            foreach (string item in singleWord)
            {
                slowa.Add(item);                
            }

            //Lista
            DisplayList(slowa);

            //Rosnaca Lista
            Console.WriteLine();
            Console.WriteLine($"List ascending order: ");            
            DisplayList(slowa.OrderBy(a => a).ToList());

            //Malejaca Lista
            Console.WriteLine();
            Console.WriteLine($"List descending order: ");
            DisplayList(slowa.OrderByDescending(a => a).ToList());

            //Zamiana na Dictionary
            Console.WriteLine();
            var dict  = slowa.ToDictionary(x => x);
            DisplayDict(dict);

            Console.WriteLine();
            Console.WriteLine("Dictionary ascending order: ");
            DisplayDict(dict.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value));

            Console.WriteLine();
            Console.WriteLine("Dictionary descending order: ");
            DisplayDict(dict.OrderByDescending(x => x.Key).ToDictionary(x => x.Key, x => x.Value));

            Console.ReadKey();
        }

        public static void DisplayList(List<string> lista)
        {
            foreach (var item in lista)
            {
                Console.WriteLine($"Element: {item}");
            }                        
        }

        public static void DisplayDict(Dictionary<string, string> slownik)
        {
            foreach (var item in slownik)
            {
                Console.WriteLine($"Dictionery Key: {item.Key}");
            }
        }
    }
}

//reactor deck unification running bulimia purchase equitable crutch freelance adjective banter evaluator receiver troublemaker lumpy star bolt residuals feral feature
