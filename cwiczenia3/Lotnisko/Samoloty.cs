﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lotnisko
{
    public class Samoloty
    {
        private Object thisLock = new Object();
        public bool[] flying = new bool[20];
        public Random rnd = new Random();
        public string[] status = new string[20];

        public void Flying(object liczba)
        {
            var i = (int)liczba;            
            flying[i] = true;
            status[i] = $"{ i + 1}: Lecę";
            Thread.Sleep(rnd.Next(1000, 10000));
            Airport(i);
        }

        public void Airport(object liczba)
        {
            var i = (int) liczba;
            lock (thisLock)
            {
                status[i] = $"{ i + 1}: UŻYWAM PASA";
                Thread.Sleep(2000);
            }
            // korzystanie z pasa
            if (flying[i] == true)
            {                
                status[i] = $"{i + 1}: Wylądowałem";
                Thread.Sleep(500);
                Hangar(i);
            }
            else
            {             
                status[i] = $"{i + 1}: Wystartowałem";
                Thread.Sleep(500);
                Flying(i);
            }
            
        }

        public void Hangar(object liczba)
        {            
            var i = (int)liczba;
            flying[i] = false;            
            status[i] = $"{i + 1}: Czekam na start";
            Thread.Sleep(rnd.Next(1000, 5000)); 
            Airport(i);
        }
    }
}
