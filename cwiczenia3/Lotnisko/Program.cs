﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lotnisko
{
    class Program
    {        
        static void Main(string[] args)
        {
            Samoloty oSamoloty = new Samoloty();          
           
            const int FLYING = 15;
            const int HANGAR = 5;

            var thread = new Thread[20];            

            // staring with 15 planes up
            for (int i = 0; i < FLYING; i++)
            {
                thread[i] = new Thread(oSamoloty.Flying);
                thread[i].Start(i);                
            }

            //starting with 5 planes down
            for (int i = FLYING; i < HANGAR+FLYING; i++)
            {
                thread[i] = new Thread(oSamoloty.Hangar);
                thread[i].Start(i);
            }

            //monitoring status
            do
            {
                Console.Clear();
                for (int i = 0; i < 20; i++)
                {
                    Console.WriteLine(oSamoloty.status[i]);
                }
                Thread.Sleep(1000);
            }
            while (Console.KeyAvailable == false);            
        }       
    }
}
