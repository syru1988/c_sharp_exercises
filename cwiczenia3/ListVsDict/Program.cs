﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListVsDict
{
    class Program
    {
        private const int LOOK_UP_INDEX = 999999;

        static void Main(string[] args)
        {
            
            var time = DateTime.Now.Millisecond;
            var lista = Filler.ListFiller();

            Console.WriteLine($"Wczytywanie Listy zajęło: {Math.Abs(time - DateTime.Now.Millisecond)} milisekund");

            time = DateTime.Now.Millisecond;
            var dict = Filler.DictFiller();
            Console.WriteLine($"Wczytywanie Dictionary zajęło: {Math.Abs(time - DateTime.Now.Millisecond)} milisekund");

            time = DateTime.Now.Millisecond;
            lista.Find(x => x >LOOK_UP_INDEX);
            Console.WriteLine($"Szukanie w liście zajęło: {Math.Abs(time - DateTime.Now.Millisecond)} milisekund");

            time = DateTime.Now.Millisecond;
            dict.First(x => x.Key > LOOK_UP_INDEX);
            Console.WriteLine($"Szukanie w dictionary zajęło: {Math.Abs(time - DateTime.Now.Millisecond)} milisekund");

            Console.ReadKey();
        }
    }
}
