﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListVsDict
{
    class Filler
    {
        private const int MAX_ROUS = 35000000;
        public static List<double> ListFiller()
        {
            var list = new List<double>();
            for(var i = 0; i < MAX_ROUS; i++)
            {
                list.Add(i*i/5);
            }
            return list;
        }

        public static Dictionary<double, double> DictFiller()
        {
            var dict = new Dictionary<double, double>();
            for (var i = 0; i < MAX_ROUS; i++)
            {
                dict.Add(i, i* i / 5);
            }
            return dict;
        }

    }
}
