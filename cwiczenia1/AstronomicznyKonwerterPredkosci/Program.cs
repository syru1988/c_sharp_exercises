﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstronomicznyKonwerterPredkosci
{
    public class Program
    {
        static void Main(string[] args)
        {
            //konwerter
            Console.WriteLine("Podaj odległość w AU: ");
            var au = Convert.ToDouble(Console.ReadLine());
            Konwerter kon1 = new Konwerter();
            Console.WriteLine(kon1.NaKm(au));

            Console.WriteLine("Podaj odległość w Km: ");
            var km = Convert.ToDouble(Console.ReadLine());
            Konwerter kon2 = new Konwerter();
            Console.WriteLine(kon2.NaAu(km));

            Console.ReadLine();

            const double MARS = 1.52;
            Console.WriteLine("Droga na Marsa: ");
            //napęd chemiczny
            double chem = kon1.NaKm(MARS) / 10 / 60 / 60 / 24;
            Console.WriteLine("Czas potrzebny na pokonanie drogi napędem chemicznym jest równy {0} dni.", chem);
            //napęd jądrowy
            double jadr = kon1.NaKm(MARS) / 12800 / 60 / 60 / 24;
            Console.WriteLine("Czas potrzebny na pokonanie drogi napędem jądrowym jest równy {0} dni.", jadr);
            //napęd termojądrowy
            double tjadr = kon1.NaKm(MARS) / 20400 / 60 / 60 / 24;
            Console.WriteLine("Czas potrzebny na pokonanie drogi napędem termojądrowym jest równy {0} dni.", tjadr);

            Console.ReadLine();
        }
    }
}
