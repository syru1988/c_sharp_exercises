﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstronomicznyKonwerterPredkosci
{
    public class Konwerter
    {
        public double NaKm(double val)
        {
            double km = val * 149597871;
            return km;
        }

        public double NaAu(double val)
        {
            double au = val / 149597871;
            return au;
        }

    }
}
