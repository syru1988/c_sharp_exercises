﻿using GMap.NET;
using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapaISS
{
    public partial class Form1 : Form
    {
        public static double latitude, longtitude;
        public class Dane
        {
            public DateTimeOffset Date { get; set; }
            public DaneGeodetic Geodetic { get; set; }
        }

        public class DaneGeodetic
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public double Altitude { get; set; }
        }

        public static async Task JsonLoad()
        {
            HttpClient client = new HttpClient()
            {
                BaseAddress = new Uri("https://tle.ivanstanojevic.me")
            };
            Dane dane = await client.GetFromJsonAsync<Dane>("/api/tle/25544/propagate");
            latitude = dane.Geodetic.Latitude;
            longtitude = dane.Geodetic.Longitude;            
        }
    

        public Form1()
        {
            _ = JsonLoad();
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            _ = JsonLoad();
            map.MinZoom = 1;
            map.MaxZoom = 16;
            map.Zoom = 3;
            
            map.MapProvider = GMapProviders.BingMap;
            map.Position = new PointLatLng(latitude, longtitude);

            longLabel.Text = "Longtitude: " + Convert.ToString(longtitude);
            latLabel.Text = "Latitude: " + Convert.ToString(latitude);
        }        
    }
}
