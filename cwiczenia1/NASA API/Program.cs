﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;


namespace NASAAPI
{
    public class Dane
    {
        public DateTimeOffset Date { get; set; }
        public DaneGeodetic Geodetic { get; set; }
    }

    public class DaneGeodetic
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
    }


    public class Program
    {
        public static async Task Main(string[] args)
        {
            HttpClient client = new HttpClient()
            {
                BaseAddress = new Uri("https://tle.ivanstanojevic.me")
            };

            do
            {
                while (!Console.KeyAvailable)
                {
                    Console.Clear();
                    Dane dane = await client.GetFromJsonAsync<Dane>("/api/tle/25544/propagate");
                    Dane dane2 = await client.GetFromJsonAsync<Dane>("/api/tle/25544");
                    Console.WriteLine("Data TLE: {0}", dane2.Date);
                    Console.WriteLine("Szerokość geograficzna: {0}", dane.Geodetic.Latitude);
                    Console.WriteLine("Długość geograficzna: {0}", dane.Geodetic.Longitude);
                    Console.WriteLine("Wysokość: {0}", dane.Geodetic.Altitude);
                    Console.WriteLine("Naciśnij ESC aby wyjść");
                    Thread.Sleep(5000);
                }
            }
            while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
